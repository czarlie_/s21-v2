console.log(`Hola Mundo 🌍`)

// Arrays
// Arrays are used to store multiple related data/values
// It is created/declared using [] brackets also known as array literals

let hobbies = ['Play video games', 'Read a book', 'Listen to music']

// Arrays make it easy to manage, manipulate and set of data
// Arrays have different methods/functions that allow us to manage our array

// Methods are functions associated with an object
// Arrays are actually a special type of object

console.log(typeof hobbies)

let grades = [75.4, 98.6, 90.34, 91.5]

const planets = ['Mercury', 'Venus', 'Mars', 'Eart']

// Alternative ways to write arrays:
let myTasks = ['Drink HTML', 'Eat Javascript', 'Bake React']

// Arrays as a best practice contains

let arraySample = ['Saitama', 'One Punch Man', 25000, true]
console.log(arraySample)
//however, since there are no problems in creating arrays like this, you may encounter exceptions to this rule in the future and in fact in other JS libraries or frameworks

// Array as a collection of data, has methods to manipulate and manage they array

//Having values with different data types might interfere or conflict with the methods of an array

// Mini Activity
// 1. Create a variable that will store an array of at least 5 of your daily routine or task
let morningRoutine = ['Check Weight', 'Hydrate', 'Skincare', 'Tidy', 'Shower']
// 2. Create a variable which will store at least 4 capital cities in the world
let capitalCities = ['Tokyo', 'Berlin', 'Oslo', 'Manila']
// 3. Log the variables in the console and send a ss in our hangouts
console.log(morningRoutine)
console.log(capitalCities)

// Each item in an array is called an element
// Array as a collection of data, as a convention, its name is usually plural

// we can also add the values of variables as elements in an array
let username1 = 'pink_princess'
let username2 = 'DareAngel'
let username3 = 'WonderfulEgg'
let username4 = 'mahalparinkita_123'

let guildMembers = [username1, username2, username3, username4]

console.log(guildMembers)

// .length property
// .length of an array tells about the number of elements inside our array
// it can actually be also set and manipulated
// .length property of an array is a number type

console.log(morningRoutine.length)
console.log(capitalCities.length)

// in fact, even strings have a .length property, which tells us the number of characters in a string
// strings are able to use some array methods and properties as well
// whitespaces are counted as characters

let fullName = 'Cardo Dalisay'
console.log(fullName.length)

// we can manipulate the .length property of an array. Being that .length property is a number that tells the total number of elements in an array
// we can also delete the last item in an array by manipulating the .length property

morningRoutine.length = morningRoutine.length - 1
console.log(morningRoutine.length)
console.log(morningRoutine)

// we could also decrement the .length property of an array
capitalCities.length--
console.log(capitalCities)

//? can we do the same trick with a string?
fullName.length = fullName.length - 1
console.log(fullName)

//? can we also add or lengthen using the same trick?
let strawHatPirates = ['Luffy', 'Zorro', 'Sanji', 'Nami']
strawHatPirates.length++
console.log(strawHatPirates)

// Learn more about this
//? When you add an array, it goes to the last element?
strawHatPirates[4] = 'Cardo'
console.log(strawHatPirates)

strawHatPirates[strawHatPirates.length] = 'Usopp'
console.log(strawHatPirates)

strawHatPirates[strawHatPirates.length] = 'Chopper'
strawHatPirates[strawHatPirates.length] = 'Franky'
console.log(strawHatPirates)
console.log(strawHatPirates.length)

// if we want to access a particular item in the array, we can do so with array indices. Each item are ordered according to their index
// NOTE: index are number types
console.log(capitalCities[0])

let lakersLegends = ['Kobe', 'Shaq', 'Lebron', 'Magic', 'Kareem']
console.log(lakersLegends[1]) //Shaq
console.log(lakersLegends[3]) //Magic

//we can also save/store a particular array element in a variable

let currentLaker = lakersLegends[2]
console.log(currentLaker)

//we can also update/re-assign the array elements using their index

lakersLegends[2] = 'Pau Gasol'
console.log(lakersLegends)

let favoriteFoods = ['Tonkatsu', 'Adobo', 'Pizza', 'Lasagna', 'Sinigang']
console.log(favoriteFoods)
//
// Mini activity #2
// 1. update/reassign the last two items in the array with your own personal faves
favoriteFoods[favoriteFoods.length - 2] = 'Egg'
favoriteFoods[favoriteFoods.length - 1] = 'Rice'
// 2. log the favoritefoods array in the console with its UPDATED elements using their index number
console.log(favoriteFoods)

/*
	Mini Activity #3
	-Create a function called addTrainers that can receive a single argument
	-add the argument in the end of theTrainers array
	-invoke and add an argument to be passed in the function
	-log theTrainer's array in the console
	-send a screenshot of your console in the Batch Hangouts

	Trainers to be added: 	Brock
							Misty
*/

let theTrainers = ['Ash']

function addTrainers(trainer) {
  theTrainers[theTrainers.length] = trainer
}

addTrainers('Misty')
addTrainers('Brock')
console.log(theTrainers)

/*
	Mini Activity #4

	Create a function named findBlackMamba which is able to receive an index number as a single argument and return the item accessed by its index
		-function should be able to receive one argument
		-return the blackMamba accessed by the index
		-create a variable outside the function called blackMamba and store the value returned by the function in it
		-log the blackMamba variable in the console
*/
function findBlackMamba(index) {
  return lakersLegends[index]
}

let blackMamba = findBlackMamba(0)
console.log(blackMamba)

// Accessing the last element of the array
let bullsLegends = ['Jordan', 'Pippen', 'Rodman', 'Rose', 'Kukoc']

//! You should review this!
let lastElementIndex = bullsLegends.length - 1
console.log(lastElementIndex)
console.log(bullsLegends.length)
console.log(bullsLegends[lastElementIndex])
console.log(bullsLegends[bullsLegends.length - 1])

// Adding items in our array
let newArr = []
console.log(newArr[0])

newArr[0] = 'Cloud Strife'
console.log(newArr)

console.log(newArr[1])
newArr[1] = 'Tifa Lockheart'
console.log(newArr)

newArr[1] = 'Aerith Gainsborough'
console.log(newArr)

newArr[newArr.length] = 'Barrat Wallace'
console.log(newArr)

newArr[newArr.length - 1] = 'Tifa Again'
console.log(newArr)

// Looping over an array
// we can loop overan array and iterate all items in the array
// set counter as the index and set a condition that as the current index iterated is less than the length of the array, we will run the loop
// it is set this way because the index starts at zero

// [0, 1, 2]
for (let index = 0; index < newArr.length; index++) console.log(newArr[index])

// check each item if they are devisible by five or not:
let numArr = [5, 12, 30, 46, 40]
for (let index = 0; index < numArr.length; index++) {
  if (numArr[index] % 5 === 0)
    console.log(`${numArr[index]} is divisible by five`)
  else console.log(`${numArr[index]} is not divisible by five`)
}

//Multidimensional Arrays

/*
	- Multidimensional Arrays are useful for storing complex data structures
	-a practical application of this is to help visualize/create real world objects
*/

let chessBoard = [
  ['a1', 'b1', 'c1', 'd1', 'e1', 'f1', 'g1', 'h1'],
  ['a2', 'b2', 'c2', 'd2', 'e2', 'f2', 'g2', 'h2'],
  ['a3', 'b3', 'c3', 'd3', 'e3', 'f3', 'g3', 'h3'],
  ['a4', 'b4', 'c4', 'd4', 'e4', 'f4', 'g4', 'h4'],
  ['a5', 'b5', 'c5', 'd5', 'e5', 'f5', 'g5', 'h5'],
  ['a6', 'b6', 'c6', 'd6', 'e6', 'f6', 'g6', 'h6'],
  ['a7', 'b7', 'c7', 'd7', 'e7', 'f7', 'g7', 'h7'],
  ['a8', 'b8', 'c8', 'd8', 'e8', 'f8', 'g8', 'h8'],
]

console.log(chessBoard)

// access elenents in a multidimentional array
console.log(chessBoard[1][4])

/*
    Mini Activity #5
    Access a8 and h6
    Log it in the console
*/
// [column][row]
console.log(chessBoard[7][0])
console.log(chessBoard[5][7])
